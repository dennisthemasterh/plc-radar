#include <Servo.h>
#include <LiquidCrystal.h>
Servo ServoOne;                    // generates an instance of a servo object
LiquidCrystal lcd(8, 7, 5, 4, 3, 2);
#define Echo_1    9//orange
#define Trigger_1 10//purple
#define Trigger_2 12
#define Echo_2    13

unsigned long EchoTime_1;
unsigned long EchoTime_2;
int  Distance_1;
int  Distance_2;
int  PulseInTimeout;
int  ServoPosition;
int  MaxDistance;
int  ServoPositionMin;
int  ServoPositionMax;
int  ServoPivotSpeed;

void setup(){
  pinMode(Echo_1, INPUT);
  pinMode(Trigger_1, OUTPUT);
  pinMode(Echo_2, INPUT);
  pinMode(Trigger_2, OUTPUT);
  ServoOne.attach(6);              // Assigns pin 6 as a servo
  ServoPosition=90;                // Default Servo position
  PulseInTimeout=8000;             // Define PulseTimeout value in microsec
  MaxDistance=150;                  // MaxDistance in cm
  ServoPositionMin=30;             // Min Servo Position
  ServoPositionMax=150;            // Max Servo Position
  ServoPivotSpeed=1;               // The Servo angle step size
  
  delay(20);
  Serial.begin(9600);
  lcd.begin(16, 2);
  lcd.print("Target is: ");
  lcd.setCursor(4, 1);
  lcd.print(" cm away");

}

void DistanceMeasurementSensor(){      // Measures Distance with Sensor 1
  digitalWrite(Trigger_1, LOW);                       // Hold Trigger Low
  delayMicroseconds(10);                              // Settle Time
  digitalWrite(Trigger_1, HIGH);                      // Enable Trigger 
  delayMicroseconds(5);                               // Hold High for 10uS
  digitalWrite(Trigger_1, LOW);                       // Hold Trigger Low to start range detect
  EchoTime_1 = pulseIn(Echo_1, HIGH, PulseInTimeout); // Timer sequence for pulse train capture
  Distance_1 = (EchoTime_1/58);
  if (Distance_1 > MaxDistance)  // Constrains Distance_1 to MaxDistance
    Distance_1 = MaxDistance;

  digitalWrite(Trigger_2, LOW);                         // Hold Trigger Low
  delayMicroseconds(10);                                // Settle Time
  digitalWrite(Trigger_2, HIGH);                        // Enable Trigger 
  delayMicroseconds(5);                                 // Hold High for 10uS
  digitalWrite(Trigger_2, LOW);                         // Hold Trigger Low to start range detect
  EchoTime_2 = pulseIn(Echo_2, HIGH, PulseInTimeout);   // Timer sequence for pulse train capture
  Distance_2 = (EchoTime_2/58);     
  if (Distance_2 > MaxDistance)    // Constrains Distance_2 to MaxDistance
    Distance_2 = MaxDistance;
}


void ObjectPresent(){                                  // Object Detection Algorithm
  if(ServoPosition<90){                                // Detection if less than 90 degrees
      if((Distance_2-Distance_1)>0)                   // Direction comparison 
       ServoPosition=ServoPosition+ServoPivotSpeed;   // Increment Servo position by ServoPivotSpeed
      if((Distance_1-Distance_2)>0)                   // Direction comparison
        ServoPosition=ServoPosition-ServoPivotSpeed;  // Increment Servo position by ServoPivotSpeed
  }
  if(ServoPosition>89){                                  // Detection if more than 89 degrees
    if((Distance_2-Distance_1)>0)                   // Direction comparison
       ServoPosition=ServoPosition+ServoPivotSpeed;   // Increment Servo position by ServoPivotSpeed
      if((Distance_1-Distance_2)>0)                   // Direction comparison
        ServoPosition=ServoPosition-ServoPivotSpeed;   // Increment Servo position by ServoPivotSpeed
  }   
  if(ServoPosition>ServoPositionMax)          // Restricts Servo position to Max
    ServoPosition=ServoPositionMax;
  if(ServoPosition<ServoPositionMin)          // Restricts Servo position to Max
    ServoPosition=ServoPositionMin;
}

void loop(){
  DistanceMeasurementSensor();    // measure both distance
  ObjectPresent();                 // Function Call
  ServoOne.write(ServoPosition);   // Rotates Servo to position stored in ServoPosition

  lcd.setCursor(0, 1);
  lcd.print((Distance_1+Distance_2)/2);
  Serial.println((Distance_1+Distance_2)/2);
  
} 


